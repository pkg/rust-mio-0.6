rust-mio-0.6 (0.6.23-3+apertis2) apertis; urgency=medium

  * Switch component from development to target to comply with Apertis
    license policy.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 16 Jan 2025 17:20:13 +0100

rust-mio-0.6 (0.6.23-3+apertis1) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Wed, 07 Jun 2023 09:55:10 +0000

rust-mio-0.6 (0.6.23-3) unstable; urgency=medium

  * Team upload.
  * Declare breaks+replaces on physical package instead of virtual one
    (Closes: #1034961)

 -- Peter Michael Green <plugwash@debian.org>  Tue, 02 May 2023 05:39:37 +0000

rust-mio-0.6 (0.6.23-2+apertis1) apertis; urgency=medium

  * Lower minimal dh-cargo version to 24

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Fri, 28 Jan 2022 10:41:10 +0000

rust-mio-0.6 (0.6.23-2+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Fri, 28 Jan 2022 10:37:48 +0000

rust-mio-0.6 (0.6.23-2) unstable; urgency=medium

  * Team upload.
  * Package mio 0.6.23 from crates.io using debcargo 2.5.0
  * Source only upload for testing migration.
  * Disable tests that are not present in the crates.io release and remove the 
    corresponding dev-dependencies so that the tests which are included can
    run.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 11 Dec 2021 19:03:21 +0000

rust-mio-0.6 (0.6.23-1) unstable; urgency=medium

  * Package mio 0.6.23 from crates.io using debcargo 2.4.4

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Thu, 02 Dec 2021 21:41:02 +0000
